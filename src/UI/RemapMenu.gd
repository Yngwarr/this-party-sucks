extends VBoxContainer

const INPUT_NAME := {
	game_up = 'Go up',
	game_down = 'Go down',
	game_left_leg = '"Move left leg"',
	game_right_leg = '"Move right leg"'
}

export (NodePath) var back_direction: NodePath

var KeyMapButton: PackedScene = preload('res://scenes/UI/KeyMapButton.tscn')

onready var Names: Container = $Split/Names
onready var Primary: Container = $Split/Buttons/Primary
onready var Secondary: Container = $Split/Buttons/Secondary
onready var Reset: Button = $Ctrls/Reset
onready var Accept: Button = $Ctrls/Accept
onready var catcher: ConfirmationDialog = $KeyCatcher
onready var back_menu: Container = get_node(back_direction)

func remappable(action: String) -> bool:
	return action.substr(0, 5) != 'game_'

func _ready() -> void:
	Accept.connect('pressed', self, 'switch_back')
	Reset.connect('pressed', self, 'reset')
	Accept.connect('visibility_changed', self, 'focus_button')
	for x in InputMap.get_actions():
		if remappable(x): continue
		var actions = InputMap.get_action_list(x)
		var label := Label.new()
		var prim: Button = KeyMapButton.instance()
		var sec: Button = KeyMapButton.instance()

		label.text = INPUT_NAME[x] if INPUT_NAME.has(x) else x
		prim.init(x, actions[0], catcher)
		sec.init(x, actions[1], catcher)

		Names.add_child(label)
		Primary.add_child(prim)
		Secondary.add_child(sec)

func _input(event) -> void:
	if !visible || catcher.visible: return
	if event.is_action_pressed('ui_cancel'):
		switch_back()
		get_tree().set_input_as_handled()

func reset() -> void:
	InputMap.load_from_globals()
	reset_names()

func reset_names() -> void:
	for i in range(Primary.get_child_count()):
		var prim = Primary.get_child(i)
		var sec = Secondary.get_child(i)
		var actions = InputMap.get_action_list(prim.action)
		prim.set_action(prim.action, actions[0])
		sec.set_action(sec.action, actions[1])

func switch_back() -> void:
	if !back_menu:
		printerr('invalid NodePath: "%s"' % back_direction)
		return
	visible = false
	back_menu.visible = true

# focuses focused_button to enable keyboard-only navigation
func focus_button():
	Accept.grab_focus()
