extends Button

func _ready() -> void:
	connect('pressed', self, 'quit_game')

func quit_game() -> void:
	get_tree().quit()
