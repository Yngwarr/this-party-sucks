extends Node2D

export (NodePath) var button_path
onready var Button = get_node(button_path)

func _ready():
	$LegsAnim.play('legs')
	$YAnim.play('move')
	Button.connect('visibility_changed', self, 'focus_button')

func focus_button():
	Button.grab_focus()
