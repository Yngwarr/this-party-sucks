extends CheckButton

export (String) var option_key: String

func _ready() -> void:
	connect('pressed', self, 'change_option')

func change_option() -> void:
	if option_key == '':
		printerr('no option key specified')
		return
	Global.set_option(option_key, pressed)
	react(pressed)

func react(pressed):
	match option_key:
		'sfx':
			var bus = AudioServer.get_bus_index('SFX')
			AudioServer.set_bus_mute(bus, !pressed)
		'music':
			var bus = AudioServer.get_bus_index('Music')
			AudioServer.set_bus_mute(bus, !pressed)
