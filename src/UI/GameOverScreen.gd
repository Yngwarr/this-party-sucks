extends CanvasLayer

onready var PlayAgain: Button = $Menu/GameOverMenu/PlayAgain
onready var Anim: AnimationPlayer = $AnimationPlayer

func _ready() -> void:
	PlayAgain.connect('visibility_changed', self, 'focus_button')

# TODO connect appear to a signal
func appear() -> void:
	Global.game_over = true
	get_tree().paused = true
	Anim.play('show')

func focus_button():
	if !PlayAgain.visible: return
	PlayAgain.grab_focus()
