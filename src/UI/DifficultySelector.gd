extends HBoxContainer

onready var buttons = get_children()

func _ready():
	switch_buttons(Global.difficulty)
	for x in buttons:
		x.connect('value_changed', self, 'button_pressed')

func button_pressed(payload: int):
	Global.difficulty = payload
	switch_buttons(payload)
	print(Global.difficulty)

func switch_buttons(level: int):
	for i in range(len(buttons)):
		buttons[i].pressed = i == level

