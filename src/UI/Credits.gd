extends Node2D

onready var Back: Button = $Back

func _ready():
	Back.connect('visibility_changed', self, 'focus_button')

func focus_button():
	Back.grab_focus()
