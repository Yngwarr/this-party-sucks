extends Button

signal value_changed

export (int) var payload

func _ready():
	connect('pressed', self, 'toggled')

func toggled():
	if pressed == false:
		pressed = true
		return
	emit_signal('value_changed', payload)
