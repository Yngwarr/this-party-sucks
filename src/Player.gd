extends KinematicBody2D

signal fallen
signal note_scored

enum State { ALIVE, HURT, FALLEN }

# dependencies must be set with the editor's UI
export (NodePath) var vinyl_path
export (NodePath) var healthbar_path
export (NodePath) var music_path

var velocity = Vector2.ZERO
var target = Vector2.ZERO
var current_state = State.ALIVE

var health = 3

onready var vinyl = get_node(vinyl_path)
onready var healthbar = get_node(healthbar_path)
onready var Music = get_node(music_path)

# child nodes
onready var clk = $Timer
onready var Anim = $AnimationPlayer
onready var AnimShader = $AnimShader
onready var AnimFace = $AnimFace

# these parameters may vary depending on the selected difficulty level
onready var leg_friction = [0.2, 0.2, 0.2][Global.difficulty]
onready var leg_boost = [0.5, 0.3, 0.25][Global.difficulty]
onready var DELTA_SPEED = [300, 300, 200][Global.difficulty]
onready var VSPEED = [300, 300, 200][Global.difficulty]

onready var VINYL_SPEED = vinyl.SPEED
onready var SPEED = VINYL_SPEED + DELTA_SPEED

var leg_velocity = 0.0
# TODO rewrite using enums for readability
var leg_was_right = true

func _ready() -> void:
	add_to_group('player')
	target = vinyl.position

	clk.connect('timeout', self, 'leg_fade')
	$Hitbox.connect('area_entered', self, 'area_entered')
	Anim.connect('animation_finished', self, 'animation_finished')
	AnimShader.connect('animation_finished', self, 'shader_animation_finished')
	AnimFace.connect('animation_finished', self, 'face_animation_finished')

	Anim.play('run')
	AnimShader.play('idle')
	AnimFace.play('idle')

func _input(_event):
	var left = Input.get_action_strength('game_left_leg')
	var right = Input.get_action_strength('game_right_leg')

	# pressing legs sequentially makes player run
	# pressing them simultaniously does nothing
	if left > 0 && right > 0: return
	if leg_was_right && left > 0:
		leg_was_right = false
		leg_velocity += leg_boost
	elif !leg_was_right && right > 0:
		leg_was_right = true
		leg_velocity += leg_boost
	if leg_velocity > 1:
		leg_velocity = 1

func leg_fade():
	leg_velocity -= leg_friction
	if leg_velocity < 0.01:
		leg_velocity = 0

func area_entered(area):
	if area.is_in_group('note'):
		emit_signal('note_scored')
		return
	if !area.is_in_group('bullet'): return
	# temporary invincibility, duration is controlled by an AnimationPlayer
	if current_state == State.HURT: return

	health -= 1
	if health <= 0:
		current_state = State.FALLEN
		Music.stop()
		Anim.play('fall')
	else:
		current_state = State.HURT
		AnimShader.play('hurt')
		healthbar.text = str(health)

func animation_finished(anim_name):
	if anim_name == 'fall':
		emit_signal('fallen')

func shader_animation_finished(anim_name):
	if anim_name == 'hurt':
		AnimShader.play('idle')
		current_state = State.ALIVE

func face_animation_finished(anim_name):
	match anim_name:
		'idle':
			AnimFace.play('blink')
			AnimFace.get_animation('idle').length = rand_range(2, 4)
		'blink':
			AnimFace.play('idle')

func _physics_process(_delta):
	var input_vec = Vector2.ZERO
	input_vec.x = Input.get_action_strength('game_up') - Input.get_action_strength('game_down')
	input_vec.y = -leg_velocity

	var vert = position.direction_to(target)
	var horiz = vert.rotated(deg2rad(90))
	var vert_scalar = VSPEED * input_vec.x
	var horiz_scalar = SPEED * input_vec.y + VINYL_SPEED
	if vert_scalar < 0 && position.distance_to(target) >= 550:
		vert_scalar = 0
	velocity = vert * vert_scalar + horiz * horiz_scalar
	velocity = move_and_slide(velocity)
