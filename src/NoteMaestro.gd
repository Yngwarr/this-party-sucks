extends Node

export (NodePath) var vinyl_path
export (PackedScene) var note

const SPAWN_POINTS = [
	Vector2(50, 0),
	Vector2(100, 0),
	Vector2(150, 0),
	Vector2(200, 0),
	Vector2(250, 0),
	Vector2(300, 0),
	Vector2(350, 0)
]

const MAX = len(SPAWN_POINTS) - 1

enum Mode {
	LINE,
	RANDOM
}

enum Dir {
	LEFT,
	RIGHT
}

var wait_time = 3
onready var vinyl = get_node(vinyl_path)
onready var Timer = $Timer
onready var Audio = $AudioStreamPlayer

var mode = Mode.LINE
var root = 0
var counter = 0
var last = 0

func _ready():
	$Despawn.add_to_group('despawn')
	Timer.connect('timeout', self, 'timeout')
	Timer.start()
	Timer.wait_time = wait_time

func timeout():
	if counter == 0:
		roll_pattern()
	spawn(SPAWN_POINTS[invoke_pattern()])

func roll_pattern():
	var roots = []
	if root > 0: roots.push_back(-1)
	if root < MAX: roots.push_back(1)
	var new_root = last + Global.sample(roots)

	var new_mode = Global.sample([Mode.LINE, Mode.RANDOM])

	root = new_root
	mode = new_mode
	counter = Global.randi_range(3, 6)

func invoke_pattern():
	counter -= 1
	var next
	match mode:
		Mode.LINE: next = root
		Mode.RANDOM: next = randi() % MAX
	last = next
	return next

func spawn(pos):
	var next = note.instance()
	next.init(vinyl, Audio)
	next.position = pos
	add_child(next)
