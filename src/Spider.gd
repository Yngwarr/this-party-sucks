extends KinematicBody2D

signal fled
signal scared

enum State { HIDDEN, IDLE, CHARGE, FLEE }

const WEB_BOUNDS = [100, 500]
const MAX_SHOTS = 3

var TIMER_BASE = [4, 3, 2][Global.difficulty]
var TIMER_DELTA = 1

var target: KinematicBody2D
var current_state = State.HIDDEN
var bullet: PackedScene

var times_shot = 0

onready var Anim = $AnimationPlayer
onready var AnimSprite = $AnimSprite
onready var ShootTimer = $ShootTimer

func _ready():
	add_to_group('foe')
	$DangerArea.connect('body_entered', self, 'body_entered')
	ShootTimer.connect('timeout', self, 'charge')
	Anim.connect('animation_finished', self, 'animation_finished')
	AnimSprite.connect('animation_finished', self, 'sprite_animation_finished')
	visible = false

func init(t: KinematicBody2D, web: PackedScene):
	target = t
	bullet = web
	adjust_animations_x()

func body_entered(body):
	if !body.is_in_group('player'): return
	if current_state != State.IDLE && current_state != State.CHARGE: return
	AnimSprite.play('fear')
	flee()
	emit_signal('scared')

func animation_finished(anim_name):
	match anim_name:
		'go':
			current_state = State.IDLE
			Anim.play('idle')
			ShootTimer.wait_time = TIMER_BASE + rand_range(0, TIMER_DELTA)
			ShootTimer.start()
		'flee':
			visible = false
			current_state = State.HIDDEN
			ShootTimer.stop()
			emit_signal('fled', self)
		'charge':
			current_state = State.IDLE
			Anim.play('idle')
			if times_shot >= MAX_SHOTS:
				flee()

func sprite_animation_finished(anim_name):
	match anim_name:
		'charge':
			AnimSprite.play('idle')
		'idle':
			AnimSprite.play('blink')
			AnimSprite.get_animation('idle').length = rand_range(1, 2)
		'blink':
			AnimSprite.play('idle')

func set_state(state):
	current_state = state

func adjust_animations_x():
	var anims = [Anim.get_animation('go'), Anim.get_animation('flee')]
	var tracks = [anims[0].find_track('.:position'), anims[1].find_track('.:position')]
	for i in range(len(tracks)):
		for j in range(anims[i].track_get_key_count(tracks[i])):
			var value = anims[i].track_get_key_value(tracks[i], j)
			anims[i].track_set_key_value(tracks[i], j, Vector2(position.x, value.y))

func set_web_length(length: float):
	var go = Anim.get_animation('go')
	var flee = Anim.get_animation('flee')
	var go_track = go.find_track('.:position')
	var flee_track = flee.find_track('.:position')
	var go_key_idx = go.track_get_key_count(go_track) - 1
	var flee_key_idx = 0
	go.track_set_key_value(go_track, go_key_idx, Vector2(position.x, length))
	flee.track_set_key_value(flee_track, flee_key_idx, Vector2(position.x, length))

# HIDDEN -> IDLE
func go():
	visible = true
	times_shot = 0
	set_web_length(Global.randi_range(WEB_BOUNDS[0], WEB_BOUNDS[1]))
	Anim.play('go')
	AnimSprite.play('idle')

func charge():
	if current_state != State.IDLE:
		ShootTimer.stop()
		return
	current_state = State.CHARGE
	Anim.play('charge')
	AnimSprite.play('charge')

func shoot():
	var web = bullet.instance()
	web.init(position.direction_to(target.position))
	owner.add_child(web)
	web.position = global_position
	times_shot += 1

func flee():
	current_state = State.FLEE
	Anim.play('flee')
