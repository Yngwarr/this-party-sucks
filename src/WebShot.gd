extends Area2D

const ALPHA := 360.0
const SPEED := 400.0

var direction: Vector2

onready var Sprite = $Sprite
onready var Anim = $AnimationPlayer

func _ready():
	add_to_group('bullet')
	connect('area_exited', self, 'area_exited')

func init(dir: Vector2):
	direction = dir.normalized()

func _physics_process(delta):
	if direction != null:
		position += direction * SPEED * delta
	Sprite.rotation_degrees += ALPHA * delta

func area_exited(area):
	if !area.is_in_group('vinyl'): return
	Anim.play('die')
