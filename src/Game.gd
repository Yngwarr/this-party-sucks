extends Node2D

var score = 0

onready var ScoreLabel = $HUD/Score
onready var FinalScore = $GameOverScreen/Menu/Score
onready var RecordLabel = $GameOverScreen/Menu/Record
onready var Player = $Player
onready var Sticker = $Vinyl/Disc/Sticker

onready var SPIDER_SCORE = [5, 6, 7][Global.difficulty]
onready var NOTE_SCORE = 5

func _ready():
	change_appearance()
	$MusicTimer.connect('timeout', $MusicPlayer, 'play')
	Player.connect('note_scored', self, 'note_scored')
	Player.connect('fallen', self, 'update_hiscore')
	Player.connect('fallen', $GameOverScreen, 'appear')
	Global.game_over = false

func change_appearance():
	var sticker = Global.get_sticker()
	var texture = load('res://img/sticker/%s.png' % sticker)
	var color = ['#3f4154', '#bd582c', '#2c91bd']
	if sticker != 34 && sticker != 45:
		color.push_back('#d3ffef')
		color.push_back('#bfc8f3')
	if sticker != 38:
		color.push_back('#f3eabf')
	Sticker.texture = texture
	VisualServer.set_default_clear_color(Color(Global.sample(color)))


func spider_scared():
	update_score(SPIDER_SCORE)

func note_scored():
	update_score(NOTE_SCORE)

func update_hiscore():
	FinalScore.text = "You scored %s points" % score
	if score > Global.hiscore:
		RecordLabel.text = "It's a new record!"
		Global.hiscore = score
	else:
		RecordLabel.text = "Your record: %s points" % Global.hiscore

func update_score(pts):
	score += pts
	ScoreLabel.text = "Score: %s" % score
