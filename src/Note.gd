extends Area2D

var vinyl
var Audio

func _ready():
	add_to_group('note')
	connect('body_entered', self, 'body_entered')

func init(vin, audio):
	vinyl = vin
	Audio = audio

func body_entered(body):
	if !body.is_in_group('player') && !body.is_in_group('despawn'):
		return
	Audio.play()
	queue_free()

func _physics_process(delta):
	var dir = position.direction_to(vinyl.position).rotated(deg2rad(90))
	var velocity = dir * (vinyl.SPEED - 50)
	position += velocity * delta
