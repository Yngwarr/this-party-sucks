extends CanvasLayer

export (int) var speed_y = 10
const HEIGHT = 600

onready var sprites = $Sprites.get_children()

func _ready():
	pass

func _process(delta):
	for s in sprites:
		s.position.y += speed_y*delta
		if s.position.y >= HEIGHT:
			s.position.y -= 2*HEIGHT

