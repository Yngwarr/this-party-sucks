extends Node

# game's options
var options := {
	sfx = true,
	music = true
}

var difficulty := 1
var hiscore := 0

var game_over := false
var current_scene = null
var first_play := true

const STICKERS = [34, 36, 37, 38, 39, 45]
var available_stickers = STICKERS

func _ready() -> void:
	randomize()
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func set_option(key: String, value) -> void:
	# TODO save options to disk
	options[key] = value

# transition between the scenes
func switch_scene(path: String) -> void:
	call_deferred('_deferred_switch_scene', path)

func _deferred_switch_scene(path: String) -> void:
	current_scene.free()

	var s = ResourceLoader.load(path)
	current_scene = s.instance()

	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)

func get_sticker():
	if len(available_stickers) == 0:
		available_stickers = STICKERS
	var s = sample(available_stickers)
	available_stickers.erase(s)
	return s

# -------- UTILS --------

# TODO move where it belongs (idk)
func event_as_text(event: InputEvent) -> String:
	if event is InputEventJoypadButton:
		return "Joypad %s" % event.button_index
	if event is InputEventJoypadMotion:
		return "Axis %s%s" % [event.axis, '-' if event.axis_value < 0 else '+']
	return event.as_text()

func randi_range(from: int, to: int) -> int:
	return randi()%(to - from + 1) + from

func sample(arr):
	var idx = randi() % len(arr)
	return arr[idx]
