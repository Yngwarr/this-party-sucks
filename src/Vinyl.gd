extends Area2D

var ALPHA = [198, 270, 468][Global.difficulty]
# 700 is insane
onready var SPEED = [150, 150, 200][Global.difficulty]

onready var Disc = $Disc

func _ready():
	add_to_group('vinyl')

func _physics_process(delta):
	Disc.rotation_degrees -= ALPHA * delta
