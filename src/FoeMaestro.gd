extends Node

onready var TIMER_BASE = [3, 3, 2][Global.difficulty]
onready var SHOWN_MAX = [3, 5, 7][Global.difficulty]

export (NodePath) var game_path
export (NodePath) var player_path
export (PackedScene) var bullet

onready var Game = get_node(game_path)
onready var Player = get_node(player_path)
onready var Timer = $Timer
onready var Foes = $Foes
onready var hidden = Foes.get_children()
onready var shown_count = 0

func _ready():
	Timer.connect('timeout', self, 'tick')
	Timer.wait_time = TIMER_BASE
	Timer.start()
	for x in hidden:
		x.init(Player, bullet)
		x.connect('fled', self, 'foe_fled')
		x.connect('scared', Game, 'spider_scared')

func tick():
	if hidden.size() == 0: return
	var next = hidden[randi() % hidden.size()]
	next.go()
	hidden.erase(next)
	shown_count += 1
	if shown_count == SHOWN_MAX:
		Timer.stop()

func foe_fled(foe):
	hidden.push_back(foe)
	shown_count -= 1
	if shown_count == SHOWN_MAX - 1:
		Timer.start()
